package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the attendance database table.
 * 
 */
@Entity
@NamedQuery(name="Attendance.findAll", query="SELECT a FROM Attendance a")
public class Attendance implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int att_ID;

	@Temporal(TemporalType.DATE)
	private Date att_date;

	private int emp_ID;

	private String status;

	public Attendance() {
	}

	public int getAtt_ID() {
		return this.att_ID;
	}

	public void setAtt_ID(int att_ID) {
		this.att_ID = att_ID;
	}

	public Date getAtt_date() {
		return this.att_date;
	}

	public void setAtt_date(Date att_date) {
		this.att_date = att_date;
	}

	public int getEmp_ID() {
		return this.emp_ID;
	}

	public void setEmp_ID(int emp_ID) {
		this.emp_ID = emp_ID;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}