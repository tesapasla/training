package model;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the employees database table.
 * 
 */
@Entity
@Table(name="employees")
@NamedQuery(name="Employee.findAll", query="SELECT e FROM Employee e")
public class Employee implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private int emp_ID;

	private float emp_basic_sal;

	@Temporal(TemporalType.DATE)
	private Date emp_dob;

	private String emp_firstname;

	private String emp_lastname;

	private String gender;

	private int pos_ID;

	public Employee() {
	}

	public int getEmp_ID() {
		return this.emp_ID;
	}

	public void setEmp_ID(int emp_ID) {
		this.emp_ID = emp_ID;
	}

	public float getEmp_basic_sal() {
		return this.emp_basic_sal;
	}

	public void setEmp_basic_sal(float emp_basic_sal) {
		this.emp_basic_sal = emp_basic_sal;
	}

	public Date getEmp_dob() {
		return this.emp_dob;
	}

	public void setEmp_dob(Date emp_dob) {
		this.emp_dob = emp_dob;
	}

	public String getEmp_firstname() {
		return this.emp_firstname;
	}

	public void setEmp_firstname(String emp_firstname) {
		this.emp_firstname = emp_firstname;
	}

	public String getEmp_lastname() {
		return this.emp_lastname;
	}

	public void setEmp_lastname(String emp_lastname) {
		this.emp_lastname = emp_lastname;
	}

	public String getGender() {
		return this.gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public int getPos_ID() {
		return this.pos_ID;
	}

	public void setPos_ID(int pos_ID) {
		this.pos_ID = pos_ID;
	}

}