package com.springcrud.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "orang")
public class Orang {
	
	@Id
	private Long NIK;
	
	@Column
	private String namaOrang;

	public Long getNIK() {
		return NIK;
	}

	public void setNIK(Long nIK) {
		NIK = nIK;
	}

	public String getNamaOrang() {
		return namaOrang;
	}

	public void setNamaOrang(String namaOrang) {
		this.namaOrang = namaOrang;
	}

	
	

}
