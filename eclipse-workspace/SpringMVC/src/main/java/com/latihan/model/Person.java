package com.latihan.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "person")
public class Person {
	
	@Id
	private long NIK;
	
	@Column
	private String NIM;

	public long getNIK() {
		return NIK;
	}

	public void setNIK(long nIK) {
		NIK = nIK;
	}

	public String getNIM() {
		return NIM;
	}

	public void setNIM(String nIM) {
		NIM = nIM;
	}
	

}
