package com.latihan.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PersonController {

	@GetMapping("/helloworld")
	public ModelAndView update(ModelMap map, Model model) {
	    model.addAttribute("hel", "Model");
	    map.compute("hell", "Model Map");
	    ModelAndView modelAndView = new MOdelAndView("hello");
	    return modelAndView;
	}
}
