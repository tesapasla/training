package com.example.demo.controller;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Attendance;
import com.example.demo.model.Employees;
import com.example.demo.model.Position;
import com.example.demo.service.AttendanceService;
import com.example.demo.service.EmployeesService;
import com.example.demo.service.PositionService;

@RestController
public class MainController {
	
	@Autowired
	private EmployeesService es;


	@GetMapping("/getempid")
	public Employees getByEmpId(@RequestParam Integer empId) {
		return  es.findByEmpID(empId);
		
	}
	
	@GetMapping("/getempall")
	public List<Employees> getEmpAll(){
		return es.findAll();
	}
	
	
	@GetMapping("/getempposid")
	public List<Employees> getEmpPosId(@RequestParam Integer posID) {
		return es.findByPosID(posID);
	}
	
	@PostMapping("/setemp")
	public Map<String, String> setEmp(@RequestBody Employees e) {
		//e =  es.setEmp(e);
		return es.setEmp(e);
	}
	
	@PostMapping("/delete")
	public Map<String, String> delEmp(@RequestBody Employees e) {
		return es.delEmp(e);
	}
	
	
	@GetMapping("/getempgender")
	public List<Employees> getEmpGender(@RequestParam String gender) {
		return es.getEmpGender(gender);
	}
	
	@PostMapping("/setgender")
	public Map<String, String> setGender(@RequestBody Employees e) {
		return es.setGender(e); 
	}
	
	@GetMapping("/getempage")
	public List<Employees> getEmpAge(@RequestParam Integer age) {
		return es.getEmpAge(age);
	}
	
//------------Position-----------
	@Autowired
	private PositionService ps;
	
	@GetMapping("/getposid")
	public Position getByPosId(@RequestParam Integer posID) {
		return ps.getByPosId(posID);
	}
	
	@GetMapping("/getposall")
	public List<Position> getPosAll(){
		return ps.getPosAll();
	}
	
	@GetMapping("/getposname")
	public List<Position> getPosName(@RequestParam String posName){
		return ps.getPosName(posName);
	}
	
	@GetMapping("/getposalw")
	public List<Position> getPosAllowance(@RequestParam Integer allowance){
		return ps.getPosAllowance(allowance);
	}
	
	@PostMapping("/setpos")
	public Map<String, String> setEmp(@RequestBody Position e) {
		return ps.setEmp(e); 
	}
	
	@PostMapping("/deletepos")
	public Map<String, String> delPos(@RequestBody Position e) {
		return ps.delPos(e); 
	}
	
	//-------------ATTENDANCE------------------
	
	@Autowired
	private AttendanceService as;
	
	 @GetMapping("/getattid")
	 public Attendance getByAttenId(@RequestParam Integer attenId) {
	  return as.getByAttenId(attenId);
	 }
	 
	 @GetMapping("/getattempid")
	 public List<Attendance> getAttEmpId(@RequestParam Integer empId){
	  return as.getAttEmpId(empId);
	 }
	 
	 @GetMapping("/getattdate")
	 public List<Attendance> getAttDate(@RequestParam Date attDate){
	  return as.getAttDate(attDate);
	 }
	 
	 @GetMapping("/getattstatus")
	 public List<Attendance> getAttStatus(@RequestParam String attStatus){
	  return as.getAttStatus(attStatus);
	 }
	 
	 @GetMapping("/getattall")
	 public List<Attendance> getAttAll(){
	  return as.getAttAll();
	 }
	 
	 @PostMapping("setatt")
	 public Map<String, String> setAtt(@RequestBody Attendance a){
	  return as.setAtt(a);
	 }
	 
	 @PostMapping("delatt")
	 public Map<String, String> delAtt(@RequestBody Attendance a){
	  return as.delAtt(a);
	 }
	
}
