package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.demo.model.Position;
import com.example.demo.repository.PositionRepo;
import com.example.demo.service.PositionService;

@Service
@Transactional
public class PositionServiceImpl implements PositionService{
	
	@Autowired
	private PositionRepo pr;
	
	public Position getByPosId(Integer posID) {
		Position pos = new Position();
		pos = pr.findByPosID(posID);
		return pos;
	}
	
	public List<Position> getPosAll(){
		List<Position> pos = new ArrayList<Position>();
		pos = (List<Position>) pr.findAll();
		return pos;
	}
	
	public List<Position> getPosName(String posName){
		List<Position> pos = new ArrayList<Position>();
		pos = (List<Position>) pr.findByPosName(posName);
		return pos;
	}
	
	public List<Position> getPosAllowance(Integer allowance){
		List<Position> pos = new ArrayList<Position>();
		pos = (List<Position>) pr.findByAllowance(allowance);
		return pos;
	}
	
	public Map<String, String> setEmp(Position e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = pr.save(e);
			respMap.put("empid", e.getPosID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "suscces");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid","");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		
		return respMap; 
	}
	
	public Map<String, String> delPos(Position e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			pr.delete(e);
			respMap.put("PosID", e.getPosID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "suscces");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("PosID","");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		
		return respMap; 
	}

}
