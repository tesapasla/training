package com.example.demo.service;

import java.sql.Date;
import java.util.List;
import java.util.Map;

import com.example.demo.model.Attendance;

public interface AttendanceService {

	public Attendance getByAttenId(Integer attenId);
	
	public List<Attendance> getAttEmpId(Integer empId);
	
	public List<Attendance> getAttDate(Date attDate);
	
	public List<Attendance> getAttStatus(String attStatus);
	
	public List<Attendance> getAttAll();
	
	public Map<String, String> setAtt(Attendance a);
	
	public Map<String, String> delAtt(Attendance a);

	
}
