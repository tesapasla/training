package com.example.demo.service;

import java.util.List;
import java.util.Map;

import com.example.demo.model.Position;

public interface PositionService {

	public Position getByPosId(Integer posID);
	
	public List<Position> getPosAll();
	
	public List<Position> getPosName(String posName);
	
	public List<Position> getPosAllowance(Integer allowance);
	
	public Map<String, String> setEmp(Position e);
	
	public Map<String, String> delPos(Position e);
	
}
