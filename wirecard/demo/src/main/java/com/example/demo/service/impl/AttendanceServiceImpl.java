package com.example.demo.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.model.Attendance;
import com.example.demo.repository.AttendanceRepo;
import com.example.demo.service.AttendanceService;

@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService{
	
	@Autowired
	private AttendanceRepo ar;
	
	
	 public Attendance getByAttenId(Integer attenId) {
	  Attendance attendance = new Attendance();
	  attendance = ar.findByAttenId(attenId);
	  return attendance;
	 }
	 
	 
	 public List<Attendance> getAttEmpId(Integer empId){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ar.findByEmpID(empId);
	  return empls; 
	 }
	 

	 public List<Attendance> getAttDate(Date attDate){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ar.findByAttDate(attDate);
	  return empls; 
	 }
	 

	 public List<Attendance> getAttStatus(String attStatus){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ar.findByAttStatus(attStatus);
	  return empls; 
	 }
	 
	 public List<Attendance> getAttAll(){
	  List<Attendance> empls = new ArrayList<Attendance>();
	  empls = (List<Attendance>) ar.findAll();
	  return empls;
	 }
	 
	 public Map<String, String> setAtt(Attendance a)
	 {
	  Map<String, String> respMap = new LinkedHashMap<String, String>();
	  try {
	   a = ar.save(a);
	   
	   respMap.put("empID", a.getAttenId(). toString());
	   respMap.put("code", HttpStatus.OK+"");
	   respMap.put("msg", "Success");
	   
	  } catch (Exception e2){
	   //TODO: handle exception
	   respMap.put("empID", "");
	   respMap.put("code", HttpStatus.CONFLICT+"");
	   respMap.put("msg", "Failed");
	  }
	  return respMap;
	 }
	 
	 public Map<String, String> delAtt(Attendance a)
	 {
	  Map<String, String> respMap = new LinkedHashMap<String, String>();
	  try {
	   ar.delete(a);
	   
	   respMap.put("empID", a.getAttenId(). toString());
	   respMap.put("code", HttpStatus.OK+"");
	   respMap.put("msg", "Success");
	   
	  } catch (Exception e2){
	   //TODO: handle exception
	   respMap.put("empID", "");
	   respMap.put("code", HttpStatus.CONFLICT+"");
	   respMap.put("msg", "Failed");
	  }
	  return respMap;
	 }

}
