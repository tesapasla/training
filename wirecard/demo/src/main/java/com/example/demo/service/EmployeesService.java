package com.example.demo.service;

import java.util.List;
import java.util.Map;


import com.example.demo.model.Employees;

public interface EmployeesService {
	
	public Employees findByEmpID(Integer empID);
	
	public List<Employees> findAll();
	
	public List<Employees> findByPosID(Integer posID);
	
	public Map<String, String> setEmp(Employees e);
	
	public Map<String, String> delEmp(Employees e);
	
	public List<Employees> getEmpGender(String gender);
	public Map<String, String> setGender(Employees e);

	public List<Employees> getEmpAge(Integer age);

}
