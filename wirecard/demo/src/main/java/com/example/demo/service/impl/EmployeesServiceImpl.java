package com.example.demo.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.example.demo.model.Employees;
import com.example.demo.repository.EmployeesRepo;
import com.example.demo.service.EmployeesService;

@Service
@Transactional
public class EmployeesServiceImpl implements EmployeesService {

	@Autowired
	private EmployeesRepo er;
	
	@Override
	public Employees findByEmpID(Integer empID) {
		// TODO Auto-generated method stub
		Employees employees = new Employees();
		employees = er.findByEmpID(empID);
		return employees;
	}
	

	@Override
	public List<Employees> findAll() {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findAll();
		return empls;
	}
		

	@Override
	public List<Employees> findByPosID(Integer posID) {
		// TODO Auto-generated method stub
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByPosID(posID);
		return empls;
	}

	@Override
	public  Map<String, String> setEmp(Employees e) {
		// TODO Auto-generated method stub
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "suscces");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid","");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		
		return respMap; 
	}

	@Override
	public Map<String, String> delEmp(Employees e) {
		// TODO Auto-generated method stub
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			er.delete(e);
			respMap.put("empid", e.getEmpID().toString());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "suscces");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("empid","");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		return respMap;
	}
	
	public List<Employees> getEmpGender(String gender) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByGender(gender);
		return empls;
	}
	
	public Map<String, String> setGender(Employees e) {
		Map<String, String> respMap = new LinkedHashMap<String, String>();
		try {
			e = er.save(e);
			respMap.put("gendernya", e.getGender());
			respMap.put("code", HttpStatus.OK+"");
			respMap.put("msg", "suscces");
		} catch (Exception e2) {
			// TODO: handle exception
			respMap.put("gender","");
			respMap.put("code", HttpStatus.CONFLICT+"");
			respMap.put("msg", "failed");
		}
		
		return respMap; 
	}
	
	public List<Employees> getEmpAge(Integer age) {
		List<Employees> empls = new ArrayList<Employees>();
		empls = (List<Employees>) er.findByAge(age);
		return empls;
	}

}
