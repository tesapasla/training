-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 08, 2019 at 11:50 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrispu`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendance`
--

CREATE TABLE `attendance` (
  `atten_id` int(11) NOT NULL,
  `Emp_ID` int(11) DEFAULT NULL,
  `Att_Date` date DEFAULT NULL,
  `Att_Status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `attendance`
--

INSERT INTO `attendance` (`atten_id`, `Emp_ID`, `Att_Date`, `Att_Status`) VALUES
(1, 2, '2019-05-06', 'Present'),
(3, 4, '2019-05-06', 'Absent'),
(4, 1, '2019-05-06', 'Present'),
(5, 2, '2019-05-06', 'Present'),
(6, 2, '2019-05-09', 'Present'),
(7, 2, '2019-05-07', 'Present'),
(8, 2, '2019-05-07', 'Present'),
(9, 2, NULL, 'Present'),
(10, 2, NULL, 'Present'),
(11, 2, NULL, 'Present'),
(12, 2, NULL, 'Present'),
(13, 2, '2019-01-07', 'Present'),
(14, 2, '2019-01-07', 'Present'),
(15, 2, '2019-01-07', 'Present'),
(16, 2, '2019-01-07', 'Present'),
(17, 2, '2019-01-07', 'Present'),
(18, 2, '2019-01-07', 'Present'),
(19, 2, '2019-01-07', 'Present'),
(20, 2, '2019-01-07', 'Present'),
(21, 2, '2019-01-07', 'Present'),
(22, 2, '2019-05-09', 'Present'),
(23, 2, '2019-05-09', 'Present'),
(24, 2, NULL, 'Present'),
(25, 2, NULL, 'Present'),
(26, 2, NULL, 'Present');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `Emp_ID` int(11) NOT NULL,
  `Emp_Name` varchar(15) NOT NULL,
  `Pos_ID` int(3) NOT NULL,
  `Gender` varchar(1) NOT NULL,
  `Age` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`Emp_ID`, `Emp_Name`, `Pos_ID`, `Gender`, `Age`) VALUES
(4, 'Fiqa', 1, 'F', 21),
(5, 'Sultan', 0, 'M', 19),
(6, 'Andre', 2, 'M', 21),
(7, 'Martin', 3, 'M', 21),
(8, 'Tessa', 3, 'F', 21);

-- --------------------------------------------------------

--
-- Table structure for table `log_attendance`
--

CREATE TABLE `log_attendance` (
  `Emp_ID` int(11) NOT NULL,
  `Att_Date` date NOT NULL,
  `Att_Status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `position`
--

CREATE TABLE `position` (
  `Pos_ID` int(11) NOT NULL,
  `Pos_Name` varchar(15) DEFAULT NULL,
  `Allowance` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `position`
--

INSERT INTO `position` (`Pos_ID`, `Pos_Name`, `Allowance`) VALUES
(1, 'Business Analys', 5000000),
(2, 'QA', 10000000),
(3, 'System Analyst', 15000000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendance`
--
ALTER TABLE `attendance`
  ADD PRIMARY KEY (`atten_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`Emp_ID`);

--
-- Indexes for table `log_attendance`
--
ALTER TABLE `log_attendance`
  ADD KEY `Emp_ID` (`Emp_ID`);

--
-- Indexes for table `position`
--
ALTER TABLE `position`
  ADD PRIMARY KEY (`Pos_ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendance`
--
ALTER TABLE `attendance`
  MODIFY `atten_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `Emp_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `position`
--
ALTER TABLE `position`
  MODIFY `Pos_ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `log_attendance`
--
ALTER TABLE `log_attendance`
  ADD CONSTRAINT `log_attendance_ibfk_1` FOREIGN KEY (`Emp_ID`) REFERENCES `employees` (`Emp_ID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
