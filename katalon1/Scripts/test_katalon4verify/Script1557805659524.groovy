import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://about.gitlab.com/')

WebUI.click(findTestObject('Object Repository/gitlab/Page_The first single application for the entire DevOps lifecycle - GitLab  GitLab/a_Sign in'))

WebUI.setText(findTestObject('Object Repository/gitlab/Page_Sign in  GitLab/input_Username or email_userlogin'), 'tesapasla@gmail.com')

WebUI.setEncryptedText(findTestObject('Object Repository/gitlab/Page_Sign in  GitLab/input_Password_userpassword'), 'OYGZFSniLFM10Z4+Vw+dAQ==')

WebUI.click(findTestObject('Object Repository/gitlab/Page_Sign in  GitLab/input_Forgot your password_commit'))

text = WebUI.getText(findTestObject('gitlab/Page_Projects  Dashboard  GitLab/h1_Projects'))

WebUI.verifyMatch(text, 'Projects', false)

WebUI.closeBrowser()

