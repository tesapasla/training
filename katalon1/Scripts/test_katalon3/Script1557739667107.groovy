import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.navigateToUrl('https://www.zalora.co.id/')

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_ZALORA Indonesia Toko Fashion Online Terlengkap di Indonesia/div_Lihat status pesanan_dy-lb-close'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_ZALORA Indonesia Toko Fashion Online Terlengkap di Indonesia/a_WANITA'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_Fashion Wanita - Jual Fashion Wanita  ZALORA Indonesia/li_Atasan'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_Pakaian Atasan Wanita - Jual Baju Atasan  ZALORA Indonesia/a_Keluar'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_Pakaian Atasan Wanita - Jual Baju Atasan  ZALORA Indonesia/div_XL_b-catalogList__itmOverlay narrow'))

CustomKeywords.'get.Screencapture.getEntirePageMobile'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_Pakaian Atasan Wanita - Jual Baju Atasan  ZALORA Indonesia/img_XL_b-catalogList__itm-img js-itm-img js-itm-hover-img-front'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_Jual Odiva Woman WILLOW ASYMMETRIC BLOUSE MOCHA Original  ZALORA Indonesia/a_Tambahkan ke wishlist'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.click(findTestObject('Object Repository/zalora/Page_Jual Odiva Woman WILLOW ASYMMETRIC BLOUSE MOCHA Original  ZALORA Indonesia/li_40'))

CustomKeywords.'get.Screencapture.getEntirePage'('')

WebUI.closeBrowser()

CustomKeywords.'database.MyConnection.closeDatabaseConnection'()

