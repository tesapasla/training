<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_XL_b-catalogList__itm-img js-itm-img js-itm-hover-img-front</name>
   <tag></tag>
   <elementGuidId>866e5512-c5c5-4e55-b4de-a7735b709d77</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//li[@id='8D2F7AA5A347A5GS']/a/span/img</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>img</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>alt</name>
      <type>Main</type>
      <value>Odiva Woman brown WILLOW ASYMMETRIC BLOUSE MOCHA 8D2F7AA5A347A5GS_1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>title</name>
      <type>Main</type>
      <value>WILLOW ASYMMETRIC BLOUSE MOCHA from Odiva Woman in brown_1</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>width</name>
      <type>Main</type>
      <value>236</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>height</name>
      <type>Main</type>
      <value>345</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>b-catalogList__itm-img js-itm-img js-itm-hover-img-front</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>src</name>
      <type>Main</type>
      <value>https://dynamic.zacdn.com/lsjuB9W5fFaGPzId2t4DgDX-ekk=/fit-in/236x345/filters:quality(90):fill(ffffff)/http://static.id.zalora.net/p/odiva-woman-8907-0522881-1.jpg</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>srcset</name>
      <type>Main</type>
      <value>https://dynamic.zacdn.com/OamMXDndy4oVn6kX9JUgufP7Z9k=/fit-in/472x690/filters:quality(90):fill(ffffff)/http://static.id.zalora.net/p/odiva-woman-8907-0522881-1.jpg 2x</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;8D2F7AA5A347A5GS&quot;)/a[@class=&quot;b-catalogList__itmLink itm-link&quot;]/span[@class=&quot;b-catalogList__itmImageWrapper js-catalogImage js-itm-hover&quot;]/img[@class=&quot;b-catalogList__itm-img js-itm-img js-itm-hover-img-front&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//li[@id='8D2F7AA5A347A5GS']/a/span/img</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='XL'])[2]/following::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='L'])[2]/following::img[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Odiva Woman'])[2]/preceding::img[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='WILLOW ASYMMETRIC BLOUSE MOCHA'])[1]/preceding::img[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:img</name>
      <type>Main</type>
      <value>//img[@alt='Odiva Woman brown WILLOW ASYMMETRIC BLOUSE MOCHA 8D2F7AA5A347A5GS_1']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/li/a/span/img</value>
   </webElementXpaths>
</WebElementEntity>
